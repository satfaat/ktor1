package dev.faat.plugins

import com.apurebase.kgraphql.GraphQL
import com.apurebase.kgraphql.KGraphQL
import dev.faat.character.data.LocDb.luke
import dev.faat.character.data.LocDb.r2d2
import dev.faat.character.model.Droid
import dev.faat.character.model.Episode
import dev.faat.character.model.Human
import io.ktor.server.application.*

fun Application.configureGraphQL() {
    install(GraphQL) {
//        configureRouting()
        playground = true
        schema {
            KGraphQL.schema {
                //configure method allows you customize schema behaviour
                configure {
                    useDefaultPrettyPrinter = true
                }

                // create query "hero" which returns instance of Character
                query("hero") {
                    resolver { episode: Episode ->
                        when (episode) {
                            Episode.NEWHOPE, Episode.JEDI -> r2d2
                            Episode.EMPIRE -> luke
                        }
                    }
                }

                // create query "heroes" which returns list of luke and r2d2
                query("heroes") {
                    resolver { -> listOf(luke, r2d2) }
                }

                // 1kotlin classes need to be registered with "type" method
                // to be included in created schema type system
                // class Character is automatically included,
                // as it is return type of both created queries
                type<Droid>()
                type<Human>()
                enum<Episode>()
            }
        }
    }
}