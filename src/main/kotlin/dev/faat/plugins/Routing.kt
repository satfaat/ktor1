package dev.faat.plugins

import dev.faat.article.routes.articleRouting
import dev.faat.customer.routes.customerRouting
import dev.faat.movie.routes.movieRouting
import dev.faat.order.routes.getOrderRoute
import dev.faat.order.routes.listOrdersRoute
import dev.faat.order.routes.totalizeOrderRoute
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.http.content.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.response.*
import io.ktor.server.routing.*


fun Application.configureRouting() {
//    install(Webjars) {
//        path = "/webjars" //defaults to /webjars
//    }
    install(StatusPages) {
        exception<Throwable> { call, cause ->
            call.respondText(text = "500: $cause", status = HttpStatusCode.InternalServerError)
        }
    }
//    install(Resources)
//    install(AutoHeadResponse)
    routing {
        // Static plugin. Try to access `/static/index.html`
        staticResources("/static", "static")
        get("/") {
            call.respondRedirect("articles")
        }
        articleRouting()
        customerRouting()
        listOrdersRoute()
        getOrderRoute()
        totalizeOrderRoute()
        movieRouting()

//        get("/webjars") {
//            call.respondText("<script src='/webjars/jquery/jquery.js'></script>", ContentType.Text.Html)
//        }
//        get("/error-test") {
//            throw IllegalStateException("Too Busy")
//        }
//        get<Articles> { article ->
//            // Get all articles ...
//            call.respond("List of articles sorted starting from ${article.sort}")
//        }
    }
}

//@Serializable
//@Resource("/articles")
//class Articles(val sort: String? = "new")
