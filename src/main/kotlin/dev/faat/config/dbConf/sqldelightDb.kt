package dev.faat.config.dbConf

import app.cash.sqldelight.db.SqlDriver
import app.cash.sqldelight.driver.jdbc.sqlite.JdbcSqliteDriver


object sqldelightDb {
    val driver: SqlDriver = JdbcSqliteDriver("jdbc:sqlite:test.db")
}