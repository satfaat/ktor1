package dev.faat.config.dbConf

import dev.faat.article.model.Articles
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction


private object DbDrivers {
    const val h2 = "org.h2.Driver"
    const val sqlite = "org.sqlite.JDBC"
}

private object URL_Path {
    const val h2 = "jdbc:h2:file:"
    const val sqlite = "jdbc:sqlite:"
}

private object DbFile {
    const val db = "./build/db"
    const val sqlite = "./build/tmp.sqlite"
}

object ExposedDb {
    fun init() {
        val database =
            Database.connect(URL_Path.sqlite + DbFile.sqlite, DbDrivers.sqlite)
        transaction(database) {
            SchemaUtils.create(Articles)
        }
    }

    suspend fun <T> dbQuery(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO) { block() }
}