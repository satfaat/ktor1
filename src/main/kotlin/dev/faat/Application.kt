package dev.faat

import dev.faat.config.dbConf.ExposedDb
import dev.faat.plugins.*
import io.ktor.server.application.*
import io.ktor.server.plugins.cors.routing.*

fun main(args: Array<String>) {
    io.ktor.server.netty.EngineMain.main(args)
}


fun Application.module() {
    install(CORS) {
        anyHost()
    }
    ExposedDb.init()
    configureSerialization()
//    configureDatabases()
    configureTemplating()
//    configureMonitoring()
//    configureHTTP()
//    configureSecurity()
//    configureGraphQL()
    configureRouting()
    configureSockets()
}
