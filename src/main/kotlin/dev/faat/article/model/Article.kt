package dev.faat.article.model


data class Article(
    val id: Int,
    val title: String,
    val body: String
)