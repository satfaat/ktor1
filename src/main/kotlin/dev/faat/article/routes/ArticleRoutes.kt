package dev.faat.article.routes

import dev.faat.article.data.dao
import dev.faat.article.model.Article
import io.ktor.server.application.*
import io.ktor.server.pebble.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.util.*


fun Route.articleRouting() {
    route("articles") {
        get {
            call.respond(PebbleContent("index.html", mapOf("articles" to dao.allArticles())))
        }
        get("new") {
            call.respond(PebbleContent("new.html", model = emptyMap()))
        }
        post {
            val formParameters = call.receiveParameters()
            val title = formParameters.getOrFail("title")
            val body = formParameters.getOrFail("body")
            val article = dao.addNewArticle(title, body)
            call.respondRedirect("/articles/${article?.id}")
        }
        get("{id}") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            val article: Article = dao.article(id)!!
            call.respond(PebbleContent("show.html", mapOf("article" to article)))
        }
        get("{id}/edit") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            val article: Article = dao.article(id)!!
            call.respond(PebbleContent("edit.html", mapOf("article" to article)))
        }
        post("{id}") {
            val id = call.parameters.getOrFail<Int>("id").toInt()
            val formParameters = call.receiveParameters()
            when (formParameters.getOrFail("_action")) {
                "update" -> {
                    val title = formParameters.getOrFail("title")
                    val body = formParameters.getOrFail("body")
                    dao.editArticle(id, title, body)
                    call.respondRedirect("/articles/$id")
                }

                "delete" -> {
                    dao.deleteArticle(id)
                    call.respondRedirect("/articles")
                }
            }
        }
    }
}