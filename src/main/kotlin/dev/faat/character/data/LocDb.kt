package dev.faat.character.data

import dev.faat.character.model.Droid
import dev.faat.character.model.Episode
import dev.faat.character.model.Human

object LocDb {
    val luke = Human("2000", "Luke Skywalker", emptyList(), Episode.entries.toSet(), "Tatooine", 1.72)
    val r2d2 = Droid("2001", "R2-D2", emptyList(), Episode.entries.toSet(), "Astromech")
}