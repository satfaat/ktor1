package dev.faat.character.model

enum class Episode {
    NEWHOPE, EMPIRE, JEDI
}