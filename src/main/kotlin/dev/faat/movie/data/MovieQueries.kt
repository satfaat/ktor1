package dev.faat.movie.data

import dev.faat.movie.model.Movie

interface MovieQueries {
    fun selectAll(): List<Movie>
    fun insertMovie(title: String, releaseDate: String)
    fun updateMovie(title: String, releaseDate: String, id: Long)
    fun deleteMovie(id: Long)
}