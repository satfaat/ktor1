package dev.faat.movie.model

import kotlinx.serialization.Serializable

@Serializable
data class Movie(
    val id: Int,
    val title: String,
    val director: String,
    val year: Int
)
