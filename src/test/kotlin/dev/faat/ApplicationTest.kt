package dev.faat

import dev.faat.plugins.configureRouting
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.server.testing.*
import kotlin.test.*

class ApplicationTest {
    @Test
    fun testRoot() = testApplication {
        application {
//            configureRouting()
            module()
        }
//        val response = client.get("/")
//
//        assertEquals(HttpStatusCode.OK, response.status)
//        assertEquals("Hello World!", response.bodyAsText())

        client.get("/").apply {
            assertEquals(HttpStatusCode.OK, status)
            assertEquals("Hello World!", bodyAsText())
        }
    }
}
