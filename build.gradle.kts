plugins {
    kotlin("jvm") version "1.9.23"
    id("io.ktor.plugin") version "2.3.10"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.9.23"
    id("app.cash.sqldelight") version "2.0.2"
}

group = "dev.faat"
version = "0.0.1"

application {
    mainClass.set("io.ktor.server.netty.EngineMain")

    val isDevelopment: Boolean = project.ext.has("development")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=$isDevelopment")
}

repositories {
    mavenCentral()
}

val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val h2_version: String by project
val KGraphQLVersion: String by project

dependencies {
    implementation(libs.ktor.core)
    implementation(libs.ktor.cors)
    implementation(libs.ktor.netty)
    implementation(libs.ktor.host.common)
    implementation(libs.ktor.websockets)
    implementation(libs.ktor.pebble)
    implementation(libs.ktor.logging)
    implementation(libs.ktor.id)
    implementation(libs.ktor.headers)
    implementation(libs.ktor.conditional.headers)
    implementation(libs.ktor.caching.headers)
    implementation(libs.ktor.compression)
    implementation(libs.ktor.webjars)
    implementation(libs.ktor.status.pages)
    implementation(libs.ktor.resources)
    implementation(libs.ktor.auto.head.response)
    implementation(libs.ktor.auth)
    implementation(libs.ktor.sessions)

    //API
    implementation(libs.ktor.openapi)
    implementation(libs.ktor.kotlinx.json)
    implementation(libs.ktor.negotiation)

    // graphql
    implementation(libs.kgraphql)
    implementation(libs.kgraphql.ktor)

    // authentication
//    implementation("io.ktor:ktor-auth:1.6.8")

    // DBs
    implementation(libs.exposed.core)
    implementation(libs.exposed.dao)
    implementation(libs.exposed.jdbc)
    // an in-memory storage
    implementation(libs.h2)
    // sqldelight
    implementation(libs.sqldelight.sqlite)

    implementation("org.webjars:jquery:3.7.1")

    implementation("ch.qos.logback:logback-classic:$logback_version")
    testImplementation(libs.ktor.tests)
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version")
}

sqldelight {
    databases {
        create("Database") {
            packageName.set("dev.faat")
        }
    }
}
