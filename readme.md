
## Src

- https://github.com/ktorio/ktor-documentation/tree/2.3.10/codeSnippets/snippets/tutorial-http-api
- https://ktor.io/docs/client-create-and-configure.html
- https://github.com/ktorio/ktor-documentation/tree/2.3.10/codeSnippets/snippets/tutorial-website-static
- https://ktor.io/docs/server-templating.html
- https://pebbletemplates.io/
- https://github.com/ktorio/ktor-documentation/tree/2.3.10/codeSnippets/snippets/tutorial-website-interactive
- https://github.com/ktorio/ktor-documentation/tree/2.3.10/codeSnippets/snippets/tutorial-websockets-server
- https://www.jetbrains.com/help/kotlin-multiplatform-dev/multiplatform-ktor-sqldelight.html?ysclid=lvc4fehrvy533494945#add-gradle-dependencies
- https://kgraphql.io/Tutorials/ktor/
- 

```shell
# for linux

cd ktor-sample-app
./gradlew build
./gradlew run
```

```powerShell
# for windows

cd ktor-sample-app
.\gradlew.bat build
.\gradlew.bat run
```